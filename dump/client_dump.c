#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct allowed {
    char name[10000];
    char password[10000];
};

int cekAllowed(char *username, char *password) {
    FILE *fp;
    struct allowed user;
    int found = 0;
    fp = fopen("../database/databases/user.dat", "rb");
    if (fp == NULL) {
        printf("Failed to open user.dat file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp) == 1) {
        if (strcmp(user.name, username) == 0 && strcmp(user.password, password) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);
    if (found == 0) {
        printf("You're Not Allowed\n");
        return 0;
    } else {
        return 1;
    }
}

void removeTimestampAndUsername(char *str) {
    char *pos = strchr(str, ':');
    if (pos != NULL) {
        char *end = strchr(pos + 1, ':');
        if (end != NULL) {
            memmove(str, end + 1, strlen(end + 1) + 1);
            
            for (int i = 0; i < strlen(str); i++) {
                if (str[i] == ':') {
                    memmove(&str[i], &str[i + 1], strlen(str) - i);
                    break;
                }
            }
        }
    }
}

void removeLeadingSpaces(char *str) {
    int i = 0, j = 0;
    while (str[i] != '\0') {
        if (str[i] != ' ') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

int main(int argc, char *argv[]) {
    int allowed = 0;
    if (geteuid() == 0) {
        allowed = 1;
    } else {
        allowed = cekAllowed(argv[2], argv[4]);
    }
    if (allowed == 0) {
        return 0;
    }

    FILE *fp;
    char lokasi[10000];
    snprintf(lokasi, sizeof(lokasi), "../database/databases/log%s.log", argv[2]);
    fp = fopen(lokasi, "r");
    if (fp == NULL) {
        printf("Failed to open log file.\n");
        return 0;
    }
    char buffer[20000];
    while (fgets(buffer, sizeof(buffer), fp)) {
        if (strstr(buffer, "CREATE TABLE") || strstr(buffer, "INSERT INTO") || strstr(buffer, "SELECT")) {
            removeTimestampAndUsername(buffer);
            removeLeadingSpaces(buffer);
            printf("%s\n", buffer);
        }
    }
    fclose(fp);
    return 0;
}
