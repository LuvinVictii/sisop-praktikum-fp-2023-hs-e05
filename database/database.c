#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

#define PORT 4444

struct table
{
	int total_column;
	char type[100][10000];
	char data[100][10000];
};

struct allowed_db
{
	char database[10000];
	char name[10000];
};

struct allowed
{
	char name[10000];
	char password[10000];
};


void createUser(char *nama, char *password);

int checkUserExist(char *username);

void insertPermission(char *nama, char *database);

int checkAllowedDb(char *nama, char *database);

int findColumn(char *table, char *kolom);

int deleteColumn(char *table, int index);

int updateColumn(char *table, int index, char *ganti);

int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where);

int deleteTableWhere(char *table, int index, char *kolom, char *where);

void writelog(char *cmd, char *nama);

int deleteTable(char *table, char *namaTable);

int main()
{
	int sockfd, ret;
	struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		printf("Connection Error.\n");
		exit(1);
	}
	printf("Socket Server Has Been Succesfully Created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
	if (ret < 0)
	{
		printf("Binding failed.\n");
		exit(1);
	}
	printf("Bind to port %d\n", 4444);

	if (listen(sockfd, 10) == 0)
	{
		printf("is working...\n");
	}
	else
	{
		printf("Binding failed.\n");
	}

	while (1)
	{
		newSocket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);
		if (newSocket < 0)
		{
			exit(1);
		}
		printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

		if ((childpid = fork()) == 0)
		{
			close(sockfd);
			while (1)
			{
				recv(newSocket, buffer, 1024, 0);
				char *token;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char cmd[100][10000];
				token = strtok(buffercopy, ":");
				int i = 0;
				char database_used[1000];
				while (token != NULL)
				{
					strcpy(cmd[i], token);
					i++;
					token = strtok(NULL, ":");
				}
				if (strcmp(cmd[0], "cUser") == 0)
				{
					if (strcmp(cmd[3], "0") == 0)
					{
						createUser(cmd[1], cmd[2]);
					}
					else
					{
						char warning[] = "Not permitted";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
					}
				}

				else if (strcmp(cmd[0], "cDatabase") == 0)
				{
					char location[20000];
					snprintf(location, sizeof location, "databases/%s", cmd[1]);
					printf("location = %s, nama = %s , database = %s\n", location, cmd[2], cmd[1]);
					mkdir(location, 0777);
					insertPermission(cmd[2], cmd[1]);
				}

				else if (strcmp(cmd[0], "gPermission") == 0)
				{
					if (strcmp(cmd[3], "0") == 0)
					{
						int exist = checkUserExist(cmd[2]);
						if (exist == 1)
						{
							insertPermission(cmd[2], cmd[1]);
						}
						else
						{
							char warning[] = "User cannot be found";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
					else
					{
						char warning[] = "You are not permitted";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
					}
				}

				else if (strcmp(cmd[0], "cekCurrentDatabase") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
					}
					send(newSocket, database_used, strlen(database_used), 0);
					bzero(buffer, sizeof(buffer));
				}

				else if (strcmp(cmd[0], "uDatabase") == 0)
				{
					if (strcmp(cmd[3], "0") != 0)
					{
						int allowed = checkAllowedDb(cmd[2], cmd[1]);
						if (allowed != 1)
						{
							char warning[] = "Database_access : You are not permitted";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
						}
						else
						{
							strncpy(database_used, cmd[1], sizeof(cmd[1]));
							char warning[] = "Database_access : Permitted";
							printf("database_used = %s\n", database_used);
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
				}

				else if (strcmp(cmd[0], "cTable") == 0)
				{
					printf("%s\n", cmd[1]);
					char *tokens;
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
					}
					else
					{
						char queryList[100][10000];
						char copycmd[20000];
						snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
						tokens = strtok(copycmd, "(), ");
						int jumlah = 0;
						while (tokens != NULL)
						{
							strcpy(queryList[jumlah], tokens);
							printf("%s\n", queryList[jumlah]);
							jumlah++;
							tokens = strtok(NULL, "(), ");
						}
						char createTable[20000];
						snprintf(createTable, sizeof createTable, "../database/databases/%s/%s", database_used, queryList[2]);
						int iterasi = 0;
						int dataIter = 3;
						struct table kolom;
						while (jumlah > 3)
						{
							strcpy(kolom.data[iterasi], queryList[dataIter]);
							printf("%s\n", kolom.data[iterasi]);
							strcpy(kolom.type[iterasi], queryList[dataIter + 1]);
							dataIter = dataIter + 2;
							jumlah = jumlah - 2;
							iterasi++;
						}
						kolom.total_column = iterasi;
						printf("iterasi = %d\n", iterasi);
						FILE *fp;
						printf("%s\n", createTable);
						fp = fopen(createTable, "ab");
						fwrite(&kolom, sizeof(kolom), 1, fp);
						fclose(fp);
					}
				}

				else if (strcmp(cmd[0], "dTable") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char hapus[20000];
					snprintf(hapus, sizeof hapus, "databases/%s/%s", database_used, cmd[1]);
					remove(hapus);
					char warning[] = "Table telah dihapus";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}

                else if (strcmp(cmd[0], "dDatabase") == 0)
				{
					int allowed = checkAllowedDb(cmd[2], cmd[1]);
					if (allowed != 1)
					{
						char warning[] = "Database_access : Anda Not permitted";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					else
					{
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", cmd[1]);
						system(hapus);
						char warning[] = "Database has been deleted";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
					}
				}

				else if (strcmp(cmd[0], "insert") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char queryList[100][10000];
					char copycmd[20000];
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					char *tokens;
					tokens = strtok(copycmd, "\'(), ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(queryList[jumlah], tokens);
						jumlah++;
						tokens = strtok(NULL, "\'(), ");
					}
					char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, queryList[2]);
					FILE *fp;
					int banyakKolom;
					fp = fopen(createTable, "r");
					if (fp == NULL)
					{
						char warning[] = "Table can not be found";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					else
					{
						struct table user;
						fread(&user, sizeof(user), 1, fp);
						banyakKolom = user.total_column;
						fclose(fp);
					}
					int iterasi = 0;
					int dataIter = 3;
					struct table kolom;
					while (jumlah > 3)
					{
						strcpy(kolom.data[iterasi], queryList[dataIter]);
						printf("%s\n", kolom.data[iterasi]);
						strcpy(kolom.type[iterasi], "string");
						dataIter++;
						jumlah = jumlah - 1;
						iterasi++;
					}
					kolom.total_column = iterasi;
					if (banyakKolom != kolom.total_column)
					{
						char warning[] = "Your input doesn't match the column";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					printf("iterasi = %d\n", iterasi);
					FILE *fp1;
					printf("%s\n", createTable);
					fp1 = fopen(createTable, "ab");
					fwrite(&kolom, sizeof(kolom), 1, fp1);
					fclose(fp1);
					char warning[] = "Data has been entered";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}

				else if (strcmp(cmd[0], "dColumn") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, cmd[2]);
					int index = findColumn(createTable, cmd[1]);
					if (index == -1)
					{
						char warning[] = "Column can not be found";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					deleteColumn(createTable, index);
					char warning[] = "Column has been deleted";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}

				else if (strcmp(cmd[0], "delete") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char queryList[100][10000];
					char copycmd[20000];
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					char *tokens;
					tokens = strtok(copycmd, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(queryList[jumlah], tokens);
						printf("%s\n", queryList[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("jumlah = %d\n", jumlah);
					char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, queryList[2]);
					if (jumlah == 3)
					{
						deleteTable(createTable, queryList[2]);
					}
					else if (jumlah == 6)
					{
						int index = findColumn(createTable, queryList[4]);
						if (index == -1)
						{
							char warning[] = "Column can not be found";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
							continue;
						}
						printf("index  = %d\n", index);
						deleteTableWhere(createTable, index, queryList[4], queryList[5]);
					}
					else
					{
						char warning[] = "Input Salah";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char warning[] = "Data telah dihapus";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}

                else if (strcmp(cmd[0], "select") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char queryList[100][10000];
					char copycmd[20000];
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					char *tokens;
					tokens = strtok(copycmd, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(queryList[jumlah], tokens);
						printf("%s\n", queryList[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("ABC\n");
					if (jumlah == 4)
					{
						char createTable[20000];
						snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, queryList[3]);
						printf("buat table = %s", createTable);
						char cmdKolom[1000];
						printf("masuk 4\n");
						if (strcmp(queryList[1], "*") == 0)
						{
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(createTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.total_column; i++)
								{
									char padding[20000];
									snprintf(padding, sizeof padding, "%s\t", user.data[i]);
									strcat(buffers, padding);
								}
								strcat(sendDatabase, buffers);
							}
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
							fclose(fp);
						}
						else
						{
							int index = findColumn(createTable, queryList[1]);
							printf("%d\n", index);
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(createTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.total_column; i++)
								{
									if (i == index)
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(buffers, padding);
									}
								}
								strcat(sendDatabase, buffers);
							}
							printf("ini send fix\n%s\n", sendDatabase);
							fclose(fp);
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
						}
					}
					else if (jumlah == 7 && strcmp(queryList[4], "WHERE") == 0)
					{
						char createTable[20000];
						snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, queryList[3]);
						printf("buat table = %s", createTable);
						char cmdKolom[1000];
						printf("masuk 4\n");
						if (strcmp(queryList[1], "*") == 0)
						{
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(createTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							int index = findColumn(createTable, queryList[5]);
							printf("%d\n", index);
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.total_column; i++)
								{
									if (strcmp(user.data[index], queryList[6]) == 0)
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(buffers, padding);
									}
								}
								strcat(sendDatabase, buffers);
							}
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
							fclose(fp);
						}
						else
						{
							int index = findColumn(createTable, queryList[1]);
							printf("%d\n", index);
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							int indexGanti = findColumn(createTable, queryList[5]);
							fp = fopen(createTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.total_column; i++)
								{
									if (i == index && (strcmp(user.data[indexGanti], queryList[6]) == 0 || strcmp(user.data[i], queryList[5]) == 0))
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(buffers, padding);
									}
								}
								strcat(sendDatabase, buffers);
							}
							printf("ini send fix\n%s\n", sendDatabase);
							fclose(fp);
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
						}
					}
					else
					{
						printf("ini query 3 %s", queryList[jumlah - 3]);
						if (strcmp(queryList[jumlah - 3], "WHERE") != 0)
						{
							char createTable[20000];
							snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, queryList[jumlah - 1]);
							printf("buat table = %s", createTable);
							printf("tanpa where");
							int index[100];
							int iterasi = 0;
							for (int i = 1; i < jumlah - 2; i++)
							{
								index[iterasi] = findColumn(createTable, queryList[i]);
								printf("%d\n", index[iterasi]);
								iterasi++;
							}
						}
						else if (strcmp(queryList[jumlah - 3], "WHERE") == 0)
						{
							printf("dengan where");
						}
					}
				}

				else if (strcmp(cmd[0], "update") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "You have not chosen database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char queryList[100][10000];
					char copycmd[20000];
					snprintf(copycmd, sizeof copycmd, "%s", cmd[1]);
					char *tokens;
					tokens = strtok(copycmd, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(queryList[jumlah], tokens);
						printf("%s\n", queryList[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("jumlah = %d\n", jumlah);
					char createTable[20000];
					snprintf(createTable, sizeof createTable, "databases/%s/%s", database_used, queryList[1]);
					if (jumlah == 5)
					{
						printf("buat table = %s, kolumn = %s", createTable, queryList[3]);
						int index = findColumn(createTable, queryList[3]);
						if (index == -1)
						{
							char warning[] = "Column can not be found";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
							continue;
						}
						printf("index = %d\n", index);
						updateColumn(createTable, index, queryList[4]);
					}
					else if (jumlah == 8)
					{
						printf("buat table = %s, kolumn = %s", createTable, queryList[3]);
						int index = findColumn(createTable, queryList[3]);
						if (index == -1)
						{
							char warning[] = "Column can not be found";
							send(newSocket, warning, strlen(warning), 0);
							bzero(buffer, sizeof(buffer));
							continue;
						}
						printf("%s\n", queryList[7]);
						int indexGanti = findColumn(createTable, queryList[6]);
						updateColumnWhere(createTable, index, queryList[4], indexGanti, queryList[7]);
					}
					else
					{
						char warning[] = "Data telah dihapus";
						send(newSocket, warning, strlen(warning), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char warning[] = "Data telah diupdate";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}


				else if (strcmp(cmd[0], "log") == 0)
				{
					writelog(cmd[1], cmd[2]);
					char warning[] = "\n";
					send(newSocket, warning, strlen(warning), 0);
					bzero(buffer, sizeof(buffer));
				}
				if (strcmp(buffer, ":exit") == 0)
				{
					printf("Terputus dari %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}
				else
				{
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}
	}
	close(newSocket);
	return 0;
}

int findColumn(char *table, char *kolom)
{
	FILE *fp;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fread(&user, sizeof(user), 1, fp);
	int index = -1;
	for (int i = 0; i < user.total_column; i++)
	{
		if (strcmp(user.data[i], kolom) == 0)
		{
			index = i;
		}
	}
	if (feof(fp))
	{
		return -1;
	}
	fclose(fp);
	return index;
}

int checkUserExist(char *username)
{
	FILE *fp;
	struct allowed user;
	int id, found = 0;
	fp = fopen("../database/databases/user.dat", "rb");
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (strcmp(user.name, username) == 0)
		{
			return 1;
		}
		if (feof(fp))
		{
			break;
		}
	}
	fclose(fp);
	return 0;
}

void createUser(char *nama, char *password)
{
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[] = {"databases/user.dat"};
	FILE *fp;
	fp = fopen(fname, "ab");
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}

int checkAllowedDb(char *nama, char *database)
{
	FILE *fp;
	struct allowed_db user;
	int id, found = 0;
	printf("nama = %s  database = %s", nama, database);
	fp = fopen("../database/databases/permission.dat", "rb");
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (strcmp(user.name, nama) == 0)
		{
			if (strcmp(user.database, database) == 0)
			{
				return 1;
			}
		}
		if (feof(fp))
		{
			break;
		}
	}
	fclose(fp);
	return 0;
}

void insertPermission(char *nama, char *database)
{
	struct allowed_db user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[] = {"databases/permission.dat"};
	FILE *fp;
	fp = fopen(fname, "ab");
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}

int deleteTable(char *table, char *namaTable)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	fread(&user, sizeof(user), 1, fp);
	int index = -1;
	struct table userCopy;
	for (int i = 0; i < user.total_column; i++)
	{
		strcpy(userCopy.data[i], user.data[i]);
		strcpy(userCopy.type[i], user.type[i]);
	}
	userCopy.total_column = user.total_column;
	fwrite(&userCopy, sizeof(userCopy), 1, fp1);
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 1;
}

int deleteTableWhere(char *table, int index, char *kolom, char *where)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
	while (1)
	{
		found = 0;
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.total_column; i++)
		{
			if (i == index && datake != 0 && strcmp(user.data[i], where) == 0)
			{
				found = 1;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.total_column = user.total_column;
		if (found != 1)
		{
			fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		}
		datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int updateColumn(char *table, int index, char *ganti)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.total_column; i++)
		{
			if (i == index && datake != 0)
			{
				strcpy(userCopy.data[iterasi], ganti);
			}
			else
			{
				strcpy(userCopy.data[iterasi], user.data[i]);
			}
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.total_column = user.total_column;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteColumn(char *table, int index)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "wb");
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.total_column; i++)
		{
			if (i == index)
			{
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.total_column = user.total_column - 1;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

void writelog(char *cmd, char *nama)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	char infoWriteLog[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, cmd);
    fputs(infoWriteLog, file);
	fclose(file);
	return;
}

int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.total_column; i++)
		{
			if (i == index && datake != 0 && strcmp(user.data[indexGanti], where) == 0)
			{
				strcpy(userCopy.data[iterasi], ganti);
			}
			else
			{
				strcpy(userCopy.data[iterasi], user.data[i]);
			}
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.total_column = user.total_column;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
