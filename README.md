| Nama                    | NRP        |
| -----------------------| -----------|
| Muhammad Daffa Harits   | 5025211005 |
| Shafa Nabilah Hanin     | 5025211222 |
| Anggara Saputra         | 5025211241 |


## A. Autentikasi

- Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.
- Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.
User root sudah ada dari awal.
Cara user root mengakses program client yaitu dengan `sudo ./client_database`
Cara menambahkan user adalah dengan `CREATE USER [nama_user] IDENTIFIED BY [password_user];`

Penjelasan:
Untuk autentikasi, program menggunakan fungsi `checkAllowed()` yang ada dalam program client.c. Fungsi ini memeriksa apakah pasangan username dan password yang diberikan oleh pengguna sesuai dengan entri yang ada dalam file `user.dat`.

Berikut adalah langkah-langkah kerja autentikasi:

1. Saat program client berjalan, terlebih dahulu dilakukan pengecekan apakah pengguna memiliki akses root atau bukan. Jika pengguna memiliki akses root (user ID 0), maka variabel `allowed` diatur menjadi 1, yang menunjukkan pengguna diizinkan.

2. Jika pengguna tidak memiliki akses root, maka fungsi `checkAllowed()` dipanggil untuk memeriksa apakah username dan password yang diberikan oleh pengguna cocok dengan entri yang ada dalam file `user.dat`. Fungsi ini menerima dua parameter: `username` dan `password`. Fungsi `checkAllowed()` membaca file `user.dat` dan membandingkan setiap entri dengan pasangan username dan password yang diberikan. Jika ditemukan pasangan yang cocok, variabel `allowed` diatur menjadi 1, yang menunjukkan pengguna diizinkan.

3. Jika variabel `allowed` bernilai 0 setelah proses autentikasi, program akan keluar dari `main()` dan tidak melanjutkan proses pengiriman perintah ke server.

Dalam hal ini, autentikasi dilakukan dengan membandingkan username dan password yang diberikan dengan entri yang ada dalam file `user.dat`. Jika pasangan username dan password cocok, pengguna dianggap diizinkan dan variabel `allowed` diatur menjadi 1, sehingga pengguna dapat melanjutkan untuk menggunakan program dan mengirim perintah ke server. Jika pasangan username dan password tidak cocok atau tidak ditemukan, variabel `allowed` tetap 0 dan program akan keluar.

## B. Autorisasi
- Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu. `USE [nama_database];`
- Yang bisa memberikan permission atas database untuk suatu user hanya root. `GRANT PERMISSION [nama_database] INTO [nama_user];`
-User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.

1. **Dalam program client.c**, terdapat beberapa bagian yang terkait dengan otorisasi:

- Bagian `else if (strcmp(cmd[0], "USE") == 0)`: Ketika pengguna memasukkan perintah `"USE"`, program client akan membuat dan mengirimkan pesan ke server dengan format `"uDatabase:nama_database:username:id_user"`. Ini digunakan untuk meminta server untuk menggunakan database dengan nama yang diberikan oleh pengguna. Otorisasi untuk akses database akan diperiksa di program server.

- Bagian `else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0)`: Ketika pengguna memasukkan perintah `"GRANT PERMISSION INTO"`, program client akan membuat dan mengirimkan pesan ke server dengan format `"gPermission:username:nama_database:id_user"`. Pesan ini digunakan untuk memberikan izin akses kepada pengguna ke database yang ditentukan. Otorisasi untuk memberikan izin akan diperiksa di program server.

2. **Dalam program database.c**, terdapat juga bagian yang terkait dengan otorisasi:

- Bagian `void insertPermission(char *nama, char *database)`: Fungsi ini digunakan untuk memasukkan entri izin akses pengguna ke dalam file "permission.dat". Ini dilakukan saat pengguna diberikan izin untuk mengakses database tertentu.

- Bagian `else if (strcmp(cmd[0], "gPermission") == 0)`: Ketika server menerima pesan "gPermission" dari program client, server akan memeriksa apakah pengguna memiliki izin untuk memberikan izin akses. Jika pengguna memiliki izin, maka izin akses akan ditambahkan ke file "permission.dat" dengan memanggil fungsi insertPermission(). Jika pengguna tidak memiliki izin, maka akan dikirimkan pesan "You are not permitted" ke program client.

- Bagian `else if (strcmp(cmd[0], "uDatabase") == 0)`: Ketika server menerima pesan "uDatabase" dari program client, server akan memeriksa izin akses pengguna terhadap database yang diminta. Jika pengguna diizinkan, maka server akan mengirimkan pesan "Database_access: Permitted" ke program client dan variabel database_used akan diatur sesuai dengan database yang digunakan. Jika pengguna tidak diizinkan, maka pesan "Database_access: You are not permitted" akan dikirimkan ke program client.

## C. Data Definition Language

- Input penamaan database, tabel, dan kolom hanya angka dan huruf.
- Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut. `CREATE DATABASE database1;`
- Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas. `CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);`
- Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan. `DROP [DATABASE | TABLE | COLUMN] [nama_database | nama_tabel | [nama_kolom] FROM [nama_tabel]];`

Dalam program yang Anda berikan, terdapat beberapa tahapan DDL (Data Definition Language) yang diimplementasikan. Berikut adalah penjelasan tentang setiap tahapan DDL:

1. **CREATE USER**: Ketika perintah "CREATE USER" diterima di program client.c, pesan dengan format "cUser:nama_user:password:id_user" akan dikirimkan ke server. Di program database.c, server akan memproses pesan tersebut dengan membuat entri pengguna baru dalam file "user.dat" menggunakan fungsi `insertUser()`. Pengguna baru akan diberi izin akses berdasarkan id_user yang diberikan.

2. **CREATE TABLE**: Ketika perintah "CREATE TABLE" diterima di program client.c, pesan dengan format "cTable:query_create_table" akan dikirimkan ke server. Di program database.c, server akan memproses pesan tersebut dengan memecah query_create_table menjadi token-token terpisah. Kemudian, server akan membuat file tabel baru dalam direktori database yang sedang digunakan, dan metadata tabel akan disimpan dalam file tersebut menggunakan struktur `table`. Informasi kolom seperti nama dan tipe kolom akan disimpan dalam file.

3. **CREATE DATABASE**: Ketika perintah "CREATE DATABASE" diterima di program client.c, pesan dengan format "cDatabase:nama_database:username:id_user" akan dikirimkan ke server. Di program database.c, server akan memproses pesan tersebut dengan membuat direktori baru dengan nama database yang diberikan dalam direktori "databases". Selain itu, entri izin akses untuk pengguna tertentu ke database baru tersebut akan ditambahkan ke file "permission.dat" menggunakan fungsi `insertPermission()`.

4. **DROP DATABASE**: Ketika perintah "DROP DATABASE" diterima di program client.c, pesan dengan format "dDatabase:nama_database:username" akan dikirimkan ke server. Di program database.c, server akan memproses pesan tersebut dengan menghapus direktori database yang sesuai, termasuk semua file dan struktur data yang terkait dengannya. Namun, sebelum menghapus database, server akan memeriksa otorisasi pengguna dengan memanggil fungsi `checkAllowedDb()` untuk memastikan pengguna memiliki izin yang sesuai.

5. **DROP TABLE**: Ketika perintah "DROP TABLE" diterima di program client.c, pesan dengan format "dTable:nama_tabel:username" akan dikirimkan ke server. Di program database.c, server akan memproses pesan tersebut dengan menghapus file tabel yang sesuai dari direktori database yang sedang digunakan. Server juga akan memeriksa otorisasi pengguna sebelum menghapus tabel.

6. **DROP COLUMN**: Ketika perintah "DROP COLUMN" diterima di program client.c, pesan dengan format "dColumn:nama_kolom:nama_tabel:username" akan dikirimkan ke server. Di program database.c, server akan memproses pesan tersebut dengan membuka file tabel yang sesuai dan menghapus kolom yang ditentukan. Server akan memeriksa otorisasi pengguna dan mencari indeks kolom yang akan dihapus menggunakan fungsi `findColumn()`.

Dalam keseluruhan, tahapan DDL tersebut memungkinkan pengguna untuk membuat dan menghapus pengguna, database, tabel, dan kolom dalam sistem database. Server melakukan verifikasi otorisasi sebelum memproses perintah dan menyimpan metadata yang diperlukan dalam file-file terkait.

## D. Data Manipulation Language

1. **Insert**

Di dalam program client.c:
1. Jika perintah yang diterima adalah "INSERT INTO", maka pesan dengan format "insert:query_insert" akan dibentuk menggunakan `snprintf`.
2. Pesan tersebut akan dikirimkan ke server menggunakan `send` dan socket clientSocket.

Di dalam program database.c:
1. Jika perintah yang diterima adalah "insert", maka server akan memproses pesan tersebut.
2. Server akan memeriksa apakah database yang sedang digunakan sudah dipilih oleh pengguna. Jika belum, server akan mengirimkan pesan "You have not chosen database" ke client dan melanjutkan iterasi berikutnya.
3. Pesan query_insert akan dipecah menjadi token-token terpisah menggunakan `strtok`. Token-token tersebut akan disimpan dalam array queryList.
4. Server akan membuka file tabel yang sesuai dengan nama tabel yang diinsertkan. Jika file tidak ditemukan, server akan mengirimkan pesan "Table can not be found" ke client dan melanjutkan iterasi berikutnya.
5. Jumlah kolom dalam file tabel akan dibaca untuk membandingkannya dengan jumlah kolom yang akan diinsertkan. Jika tidak sesuai, server akan mengirimkan pesan "Your input doesn't match the column" ke client dan melanjutkan iterasi berikutnya.
6. Data kolom yang akan diinsertkan akan disimpan dalam struktur `table` dan akan ditulis ke dalam file tabel.
7. Server akan mengirimkan pesan "Data has been entered" ke client.

2. **Update**

Di dalam program client.c:
1. Jika perintah yang diterima adalah "UPDATE", maka pesan dengan format "update:query_update" akan dibentuk menggunakan `snprintf`.
2. Pesan tersebut akan dikirimkan ke server menggunakan `send` dan socket clientSocket.

Di dalam program database.c:
1. Jika perintah yang diterima adalah "update", maka server akan memproses pesan tersebut.
2. Server akan memeriksa apakah database yang sedang digunakan sudah dipilih oleh pengguna. Jika belum, server akan mengirimkan pesan "You have not chosen database" ke client dan melanjutkan iterasi berikutnya.
3. Pesan query_update akan dipecah menjadi token-token terpisah menggunakan `strtok`. Token-token tersebut akan disimpan dalam array queryList.
4. Server akan membuka file tabel yang sesuai dengan nama tabel yang akan diupdate. Jika file tidak ditemukan, server akan mengirimkan pesan "Table can not be found" ke client dan melanjutkan iterasi berikutnya.
5. Server akan memeriksa jumlah token dalam queryList untuk menentukan jenis operasi update yang akan dilakukan:
   - Jika jumlah token adalah 5, itu berarti operasi update pada satu kolom tanpa klausa WHERE. Server akan mencari indeks kolom yang akan diupdate menggunakan `findColumn` dan memanggil `updateColumn` untuk memperbarui nilainya.
   - Jika jumlah token adalah 8, itu berarti operasi update pada satu kolom dengan klausa WHERE. Server akan mencari indeks kolom yang akan diupdate dan indeks kolom yang digunakan sebagai klausa WHERE menggunakan `findColumn` dan memanggil `updateColumnWhere` untuk memperbarui nilainya berdasarkan klausa WHERE.
   - Jika jumlah token tidak sesuai dengan kedua kondisi di atas, server akan mengirimkan pesan "Data telah dihapus" ke client dan melanjutkan iterasi berikutnya.
6. Setelah operasi update selesai dilakukan, server akan mengirimkan pesan "Data telah diupdate" ke client.

3. **Delete**

Di dalam program client.c:
1. Jika perintah yang diterima adalah "DELETE", maka pesan dengan format "delete:query_delete" akan dibentuk menggunakan `snprintf`.
2. Pesan tersebut akan dikirimkan ke server menggunakan `send` dan socket clientSocket.

Di dalam program database.c:
1. Jika perintah yang diterima adalah "delete", maka server akan memproses pesan tersebut.
2. Server akan memeriksa apakah database yang sedang digunakan sudah dipilih oleh pengguna. Jika belum, server akan mengirimkan pesan "You have not chosen database" ke client dan melanjutkan iterasi berikutnya.
3. Pesan query_delete akan dipecah menjadi token-token terpisah menggunakan `strtok`. Token-token tersebut akan disimpan dalam array queryList.
4. Server akan membentuk path file tabel yang sesuai dengan nama tabel yang akan dihapus berdasarkan database yang sedang digunakan.
5. Server akan memeriksa jumlah token dalam queryList untuk menentukan jenis operasi delete yang akan dilakukan:
   - Jika jumlah token adalah 3, itu berarti operasi delete seluruh tabel tanpa klausa WHERE. Server akan memanggil fungsi `deleteTable` untuk menghapus tabel.
   - Jika jumlah token adalah 6, itu berarti operasi delete dengan klausa WHERE. Server akan mencari indeks kolom yang digunakan sebagai klausa WHERE menggunakan `findColumn` dan memanggil fungsi `deleteTableWhere` untuk menghapus baris berdasarkan klausa WHERE.
   - Jika jumlah token tidak sesuai dengan kedua kondisi di atas, server akan mengirimkan pesan "Input Salah" ke client dan melanjutkan iterasi berikutnya.
6. Setelah operasi delete selesai dilakukan, server akan mengirimkan pesan "Data telah dihapus" ke client.

Dalam implementasinya, terdapat beberapa fungsi tambahan yang digunakan dalam database.c:
- `deleteTable`: Menghapus seluruh tabel dengan memanfaatkan fungsi `fwrite` untuk menulis ulang data tabel ke file sementara, kemudian menghapus file asli dan mengganti namanya dengan file sementara.
- `deleteTableWhere`: Menghapus baris dalam tabel berdasarkan klausa WHERE. Data baris yang tidak sesuai dengan klausa WHERE akan ditulis ulang ke file sementara menggunakan fungsi `fwrite`, kemudian file asli dihapus dan diganti namanya dengan file sementara.
- `deleteColumn`: Menghapus kolom dalam tabel dengan memanfaatkan fungsi `fwrite` untuk menulis ulang data tabel ke file sementara, kemudian menghapus file asli dan mengganti namanya dengan file sementara.

4. **Select**

Berikut adalah penjelasan perintah select yang ada pada program `client.c`

1. Jika perintah yang diterima adalah "SELECT", maka pesan dengan format "select:query_select" akan dibentuk menggunakan fungsi `snprintf`. Pesan tersebut akan diisi dengan perintah SELECT dan query_select yang merupakan perintah SELECT yang dikirim oleh pengguna.
2. Pesan yang telah dibentuk akan dikirimkan ke server menggunakan fungsi `send` dengan menggunakan socket `clientSocket`.

5. **Where**

Dalam kode yang Anda berikan, terdapat implementasi dua operasi DML (Data Manipulation Language) pada program `database.c`, yaitu UPDATE dan DELETE. Berikut adalah penjelasan tentang masing-masing operasi:

1. **updateColumnWhere**:
   - Fungsi `updateColumnWhere` digunakan untuk melakukan operasi UPDATE pada tabel di mana nilai kolom tertentu akan diubah berdasarkan kondisi tertentu.
   - Fungsi ini menerima argumen seperti `table` (nama tabel), `index` (indeks kolom yang akan diubah), `ganti` (nilai baru yang akan digunakan), `indexGanti` (indeks kolom yang digunakan sebagai kondisi), dan `where` (nilai yang harus cocok dengan kondisi).
   - Fungsi ini membuka file tabel dengan mode membaca biner (`rb`) dan file temporary (`temp`) dengan mode menambahkan biner (`ab`).
   - Kemudian, fungsi ini membaca setiap baris data dari tabel. Jika mencapai akhir file (`feof(fp)`), proses dibreak.
   - Pada setiap iterasi, fungsi ini mengecek kondisi jika `i == index` (indeks kolom yang akan diubah) dan `datake != 0` (untuk melewati baris header tabel). Jika kondisi terpenuhi dan nilai kolom `indexGanti` sama dengan `where`, nilai kolom tersebut diubah menjadi `ganti` pada baris data yang sedang diproses.
   - Selain itu, nilai kolom lainnya disalin dari baris data asli ke baris data yang baru.
   - Baris data yang telah dimodifikasi disimpan di file temporary (`temp`).
   - Setelah selesai membaca semua baris data, file tabel asli dihapus (`remove(table)`) dan file temporary diubah namanya menjadi file tabel asli (`rename("temp", table)`).

2. **deleteTableWhere**:
   - Fungsi `deleteTableWhere` digunakan untuk melakukan operasi DELETE pada tabel di mana baris data tertentu akan dihapus berdasarkan kondisi tertentu.
   - Fungsi ini menerima argumen seperti `table` (nama tabel), `index` (indeks kolom yang digunakan sebagai kondisi), `kolom` (nama kolom yang digunakan sebagai kondisi), dan `where` (nilai yang harus cocok dengan kondisi).
   - Fungsi ini juga membuka file tabel dengan mode membaca biner (`rb`) dan file temporary (`temp`) dengan mode menambahkan biner (`ab`).
   - Kemudian, fungsi ini membaca setiap baris data dari tabel. Jika mencapai akhir file (`feof(fp)`), proses dibreak.
   - Pada setiap iterasi, fungsi ini mengecek kondisi jika `i == index` (indeks kolom yang digunakan sebagai kondisi) dan `datake != 0` (untuk melewati baris header tabel). Jika kondisi terpenuhi dan nilai kolom `index` sama dengan `where`, baris data tersebut tidak disalin ke file temporary (`temp`), sehingga dihapus dari hasil akhir.
   - Jika kondisi tidak terpenuhi, baris data tersebut disalin ke file temporary.
   - Setelah selesai membaca semua baris data, file tabel asli dihapus (`remove(table)`) dan file temporary diubah namanya menjadi file tabel asli (`rename("temp", table)`).

## E. Logging

1. **client.c**:
   - Fungsi `writelog` digunakan untuk melakukan logging ke file log.
   - Fungsi ini menerima argumen `cmd` (perintah) dan `nama` (nama pengguna).
   - Pertama, fungsi ini mendapatkan waktu saat ini menggunakan fungsi `time()` dan mengonversinya ke struktur `tm` menggunakan fungsi `localtime()`.
   - Selanjutnya, fungsi membuka file log dengan mode menambahkan biner (`ab`) menggunakan `fopen`.
   - Kemudian, fungsi memformat informasi log yang akan ditulis ke dalam string `infoWriteLog` menggunakan `sprintf()`. Formatnya adalah "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", yang mencakup tahun, bulan, tanggal, jam, menit, detik, nama pengguna, dan perintah.
   - Informasi log ditulis ke file menggunakan `fputs()`, dan file log ditutup menggunakan `fclose()`.

2. **database.c**:
   - Fungsi `writelog` di sini juga digunakan untuk melakukan logging ke file log, dengan argumen `cmd` (perintah) dan `nama` (nama pengguna).
   - Fungsi ini mirip dengan implementasi di `client.c`, namun ada perbedaan dalam format informasi log yang ditulis.
   - Fungsi ini juga mendapatkan waktu saat ini dan mengonversinya ke struktur `tm`.
   - Fungsi membuka file log dengan mode menambahkan biner (`ab`).
   - Informasi log diformat dalam string `infoWriteLog` menggunakan `sprintf()`. Formatnya adalah "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", yang mencakup tahun, bulan, tanggal, jam, menit, detik, nama pengguna, dan perintah.
   - Informasi log ditulis ke file menggunakan `fputs()`, dan file log ditutup menggunakan `fclose()`.

## F. Reliability

Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel. 
`./[program_dump_database] -u [username] -p [password] [nama_database]`

Pada file `client_dump.c`, terdapat implementasi untuk membaca log dari file log pengguna dan mencetak perintah yang relevan (seperti "CREATE TABLE", "INSERT INTO", dan "SELECT"). Ini membantu dalam menampilkan riwayat aktivitas pengguna terkait operasi yang terjadi dalam basis data. Berikut penjelasan untuk komponen-komponen utama dalam kode tersebut:

1. Fungsi `cekAllowed`:
   - Fungsi ini digunakan untuk memeriksa apakah pengguna diizinkan atau tidak.
   - Fungsi menerima argumen `username` dan `password`.
   - Fungsi membuka file `user.dat` (yang berisi informasi pengguna yang diizinkan) dalam mode membaca biner (`rb`).
   - Selama membaca record dari file, fungsi membandingkan `username` dan `password` dengan setiap record yang ada dalam file.
   - Jika ada kecocokan, maka pengguna dianggap diizinkan (`found` diset menjadi 1).
   - Setelah selesai membaca file, file ditutup dan fungsi mengembalikan nilai 0 jika pengguna tidak diizinkan dan 1 jika pengguna diizinkan.

2. Fungsi `removeTimestampAndUsername`:
   - Fungsi ini digunakan untuk menghapus timestamp dan username dari baris log.
   - Fungsi menerima argumen `str` yang merupakan baris log.
   - Pertama, fungsi mencari karakter `':'` pertama dalam string `str` menggunakan `strchr`.
   - Jika karakter ditemukan, maka fungsi mencari karakter `':'` berikutnya menggunakan `strchr` dan menggeser seluruh teks setelah karakter tersebut ke awal string menggunakan `memmove`.
   - Kemudian, fungsi melakukan iterasi pada string untuk menemukan dan menghapus karakter `':'` berikutnya agar tersisa hanya perintah yang relevan.

3. Fungsi `removeLeadingSpaces`:
   - Fungsi ini digunakan untuk menghapus spasi awal pada sebuah string.
   - Fungsi menerima argumen `str` yang merupakan string yang akan diproses.
   - Fungsi menggunakan dua indeks (`i` dan `j`) untuk menggeser karakter-karakter yang bukan spasi ke awal string.
   - Setiap kali karakter bukan spasi ditemukan, karakter tersebut dipindahkan ke posisi baru menggunakan `str[j++] = str[i]`.
   - Setelah selesai, fungsi mengakhiri string dengan karakter null (`'\0'`) pada posisi `j` yang terakhir.

4. Fungsi `main`:
   - Fungsi utama yang melakukan langkah-langkah berikut:
     - Memeriksa izin akses (apakah dijalankan dengan hak akses root atau melalui izin pengguna yang diizinkan menggunakan fungsi `cekAllowed`).
     - Membuka file log pengguna menggunakan `fopen` dengan mode membaca (`r`).
     - Jika file berhasil dibuka, fungsi membaca baris-baris log menggunakan `fgets`.
     - Untuk setiap baris log yang dibaca, fungsi memeriksa keberadaan kata kunci seperti "CREATE TABLE", "INSERT INTO", atau "SELECT".
     - Jika kata kunci tersebut ditemukan, fungsi menghapus timestamp dan username dari baris log menggunakan fungsi `removeTimestampAndUsername` dan menghapus spasi awal menggunakan `removeLeadingSpaces`.
     - Akhirnya, fungsi mencetak baris log yang relevan ke layar.
     - Setelah selesai membaca file, file ditutup menggunakan `fclose`.

## G. Tambahan

## H. Error Handling
Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.

## I. Containerization

- **Dockerfile**
Dockerfile tersebut adalah file yang digunakan untuk membangun sebuah image Docker. Berikut adalah penjelasan untuk setiap perintah yang ada dalam Dockerfile:

1. `FROM ubuntu:latest`: Mendefinisikan base image yang akan digunakan. Dalam hal ini, menggunakan image Ubuntu terbaru sebagai dasar.

2. `RUN apt-get update && apt-get install -y gcc make`: Menjalankan perintah apt-get untuk mengupdate paket dan menginstall dependensi yang dibutuhkan. Di sini, dipasang gcc (compiler) dan make (build automation tool).

3. `WORKDIR /app`: Mengatur direktori kerja di dalam container menjadi `/app`.

4. `COPY database/ /app/database/`: Menyalin seluruh konten dari direktori `database` di host ke dalam direktori `/app/database/` di dalam container.

5. `COPY client/ /app/client/`: Menyalin seluruh konten dari direktori `client` di host ke dalam direktori `/app/client/` di dalam container.

6. `COPY dump/ /app/dump/`: Menyalin seluruh konten dari direktori `dump` di host ke dalam direktori `/app/dump/` di dalam container.

7. `RUN mkdir -p /app/database/databases`: Membuat direktori `/app/database/databases` di dalam container.

8. `RUN gcc -o /app/database/database /app/database/database.c`: Mengompilasi program `database.c` yang berada di dalam direktori `/app/database/` dan menghasilkan file biner `database` di direktori `/app/database/` di dalam container.

9. `RUN gcc -o /app/client/client /app/client/client.c`: Mengompilasi program `client.c` yang berada di dalam direktori `/app/client/` dan menghasilkan file biner `client` di direktori `/app/client/` di dalam container.

10. `RUN gcc -o /app/dump/client_dump /app/dump/client_dump.c`: Mengompilasi program `client_dump.c` yang berada di dalam direktori `/app/dump/` dan menghasilkan file biner `client_dump` di direktori `/app/dump/` di dalam container.

11. `CMD ["/bin/bash"]`: Mengatur perintah yang akan dijalankan ketika container berjalan. Di sini, mengatur agar container menjalankan shell `/bin/bash`.

Dockerfile ini akan membangun image Docker yang berisi aplikasi-aplikasi yang telah dikompilasi dan disiapkan di dalam container. Image tersebut dapat digunakan untuk menjalankan container yang menjalankan aplikasi-aplikasi tersebut.

- **Dockerhub**
`https://hub.docker.com/r/shafanh/storage-app`

- **Dockercompose**
Untuk memastikan sistem kalian mampu menangani peningkatan penggunaan, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru dan jalankan Docker Compose di sana.

```c
version: "3"

services:
  sukolilo:
    build:
      context: ./sukolilo
      dockerfile: Dockerfile
    container_name: sukolilo_cnt

  keputih:
    build:
      context: ./keputih
      dockerfile: Dockerfile
    container_name: keputih_cnt

  gebang:
    build:
      context: ./gebang
      dockerfile: Dockerfile
    container_name: gebang_cnt

  mulyos:
    build:
      context: ./mulyos
      dockerfile: Dockerfile
    container_name: mulyos_cnt

  semolowaru:
    build:
      context: ./semolowaru
      dockerfile: Dockerfile
    container_name: semolowaru_cnt
```
    
## J. Extra

## Kendala
Saat program akan dijalankan lagi, di awal proses menguhubungkan ke socket sudah tidak bisa
![error](img/error.png)